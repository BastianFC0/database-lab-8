import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.Types;
import java.sql.SQLException;
import java.sql.*;


public class JustLeeServices{
    //--1

    public Connection getConnection() throws SQLException {
        Connection conn = null;
            conn = DriverManager.getConnection("jdbc:oracle:thin:@pdbora19c.dawsoncollege.qc.ca:1521/pdbora19c.dawsoncollege.qc.ca","A2041556","Arceus5417");
        System.out.println("Connected to the database");
        return conn;
    }
    //--2

    public Book populateBook(String isbn) throws SQLException{
        String query = "SELECT isbn, title, pubdate, pubid, cost, retail, discount, category FROM BOOKS WHERE ISBN = "+isbn;
        Statement statement = getConnection().createStatement();
        ResultSet resultset =  statement.executeQuery(query);
        resultset.next();
            String rIsbn = resultset.getString("isbn");
            String rTitle = resultset.getString("title");
            String rDate = resultset.getString("pubdate");
            int rPubId = resultset.getInt("pubid");
            double rCost = resultset.getDouble("cost");
            double rRetail = resultset.getDouble("retail");
            double rDiscount = resultset.getDouble("discount");
            String category = resultset.getString("category");
        String query2 = "SELECT * FROM PUBLISHER WHERE PUBID = "+rPubId;
        resultset = statement.executeQuery(query2);
        resultset.next();
            int pubId = resultset.getInt("pubid");
            String name = resultset.getString("name");
            String contact = resultset.getString("contact");
            String phone = resultset.getString("phone");
            Publisher publisher = new Publisher(pubId, name, contact, phone);
        String query3 = "SELECT AUTHORID FROM BOOKAUTHOR WHERE ISBN = "+rIsbn;
        resultset = statement.executeQuery(query3);
        resultset.next();
            String authorId = resultset.getString("AUTHORID");
        String query4 = "SELECT * FROM AUTHOR WHERE AUTHORID = "+"'"+authorId+"'";
        resultset = statement.executeQuery(query4);
        resultset.next();
            String lName = resultset.getString("lname");
            String fName = resultset.getString("fname");
            Authors author = new Authors(authorId, lName, fName);
        Book book = new Book(rIsbn, rTitle, rDate, rPubId, rCost, rRetail
        , rDiscount, category, author, publisher);
        statement.close();
        System.out.println("Closing connection");
        return book;
    }
    //--4 

    public void addBook(Book book) throws SQLException{
        Connection conn = getConnection();
        String query = "INSERT INTO BOOKS VALUES('"+book.getIsbn()+"','"+book.getTitle()+"','"+book.getDate()+"',"+book.getPubId()
        +","+book.getCost()+","+book.getRetail()+","+book.getDiscount()+",'"+book.getCategory()+"')";
        conn.setAutoCommit(false);
            Statement statement = conn.createStatement();
            statement.executeUpdate(query);
            statement.close();
            System.out.println("Closing connection");
        conn.commit();
    }

    //--6

    public Customer getCustomerObject(String id) throws SQLException{
        String query = "SELECT * FROM CUSTOMERS WHERE customer# = "+id;
        Connection conn = getConnection();
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        resultSet.next();
            String lastName = resultSet.getString("lastname");
            String firstName = resultSet.getString("firstname");
            Address address = new Address(resultSet.getString("address"),resultSet.getString("city"),
            resultSet.getString("state"),resultSet.getString("zip"),resultSet.getString("region"));
            String referred = resultSet.getString("referred");
            Customer customer = new Customer(id, lastName, firstName, address, referred);
        statement.close();
        System.out.println("Closing connection");
        return customer;
    }

    //--8

    public Order placeOrder(Customer customer, Book book) throws SQLException{
        String query = "SELECT MAX(ORDER#) as order# FROM ORDERS";
        Connection conn = getConnection();
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(query);
        resultSet.next();
            int orderId = resultSet.getInt("order#")+1;
            String customerId = customer.getId();
        query = "SELECT SYSDATE FROM DUAL";
        resultSet = statement.executeQuery(query);
        resultSet.next();
            String orderDate = resultSet.getString("sysdate");
        query = "SELECT SYSDATE+1 FROM DUAL";
        resultSet = statement.executeQuery(query);
        resultSet.next();
            String shipDate = resultSet.getString("sysdate+1");
            String shipStreet = customer.getAddress().getAddress();
            String shipCity = customer.getAddress().getCity();
            String shipState = customer.getAddress().getCity();
            String shipZip = customer.getAddress().getZip();
            String shipCost = "1";
        Order order = new Order(orderId, customerId, orderDate, shipDate, shipStreet, shipCity, shipState, shipZip, shipCost);
        statement.close();
        System.out.println("Closing connection");
        return order;
    }
}