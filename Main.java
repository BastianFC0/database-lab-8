import java.sql.SQLException;

public class Main {
    public static void main(String[] args) throws SQLException{
        JustLeeServices j = new JustLeeServices();
        Customer customer = j.getCustomerObject("1001");
        Book book = j.populateBook("1059831198");
        System.out.println(j.placeOrder(customer, book).getCustomerId());
        }
    }

