public class Authors {
    private String authorID;
    private String lName;
    private String fName;
    public Authors(String authorID, String lName, String fName) {
        this.authorID = authorID;
        this.lName = lName;
        this.fName = fName;
    }
    public String getAuthorID() {
        return authorID;
    }
    public void setAuthorID(String authorID) {
        this.authorID = authorID;
    }
    public String getlName() {
        return lName;
    }
    public void setlName(String lName) {
        this.lName = lName;
    }
    public String getfName() {
        return fName;
    }
    public void setfName(String fName) {
        this.fName = fName;
    }
    
}
