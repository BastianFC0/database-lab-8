//--5

public class Address {
    private String address;
    private String city;
    private String state;
    private String zip;
    private String region;

    public Address(String address, String city, String state, String zip, String region) {
        this.address = address;
        this.city = city;
        this.state = state;
        this.zip = zip;
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }
    
}
