public class Order {
    private int orderId;
    private String customerId;
    private String orderDate;
    private String shipDate;
    private String shipStreet;
    private String shipCity;
    private String shipState;
    private String shipZip;
    private String shipCost;
    public Order(int orderId, String customerId, String orderDate, String shipDate, String shipStreet,
            String shipCity, String shipState, String shipZip, String shipCost) {
        this.orderId = orderId;
        this.customerId = customerId;
        this.orderDate = orderDate;
        this.shipDate = shipDate;
        this.shipStreet = shipStreet;
        this.shipCity = shipCity;
        this.shipState = shipState;
        this.shipZip = shipZip;
        this.shipCost = shipCost;
    }
    public int getOrderId() {
        return orderId;
    }
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
    public String getCustomerId() {
        return customerId;
    }
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
    public String getOrderDate() {
        return orderDate;
    }
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }
    public String getShipDate() {
        return shipDate;
    }
    public void setShipDate(String shipDate) {
        this.shipDate = shipDate;
    }
    public String getShipStreet() {
        return shipStreet;
    }
    public void setShipStreet(String shipStreet) {
        this.shipStreet = shipStreet;
    }
    public String getShipCity() {
        return shipCity;
    }
    public void setShipCity(String shipCity) {
        this.shipCity = shipCity;
    }
    public String getShipState() {
        return shipState;
    }
    public void setShipState(String shipState) {
        this.shipState = shipState;
    }
    public String getShipZip() {
        return shipZip;
    }
    public void setShipZip(String shipZip) {
        this.shipZip = shipZip;
    }
    public String getShipCost() {
        return shipCost;
    }
    public void setShipCost(String shipCost) {
        this.shipCost = shipCost;
    }
    
}
