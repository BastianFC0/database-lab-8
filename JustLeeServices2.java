import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.Types;
import java.sql.SQLException;
import java.sql.*;

public class JustLeeServices2 {

    //--7

    public Book[] getPopularBooks() throws SQLException{
        JustLeeServices j = new JustLeeServices();
        Connection conn = j.getConnection();
        Statement statement = conn.createStatement();
        String query;
        ResultSet resultSet; 
        Book[] bookArray = new Book[5];
        for(int i = 0; i < 5; i++){
            query = "SELECT ISBN FROM (SELECT ISBN, ROWNUM AS RN FROM "+
            "(SELECT ISBN, COUNT(QUANTITY) FROM ORDERITEMS GROUP BY ISBN "+
                "ORDER BY COUNT(QUANTITY) DESC)) WHERE RN = "+(i+1);
            resultSet = statement.executeQuery(query);
            resultSet.next();
            bookArray[i] = j.populateBook(resultSet.getString("isbn"));
        }
        return bookArray;
    }
}
