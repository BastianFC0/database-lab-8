Database Lab 8

Most numbers are in JustLeeServices.java. If they are not there, they are in their respective classes. The only one that doesn't follow this rule is the one for number 7, which is in JustLeeServices2.java because calling the method populateBooks() is much easier than copying everything.
