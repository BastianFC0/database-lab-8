//--2

public class Book {
    private String isbn;
    private String title;
    private String date;
    private int pubId;
    private double cost;
    private double retail;
    private double discount;
    private String category;
    private Authors author;
    private Publisher publisher;
    public Book(String isbn, String title, String date, int pubId, double cost, double retail, double discount,
            String category, Authors author, Publisher publisher) {
        this.isbn = isbn;
        this.title = title;
        this.date = date;
        this.pubId = pubId;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
        this.author = author;
        this.publisher = publisher;
    }
    public String getIsbn() {
        return isbn;
    }
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getDate() {
        return date;
    }
    public void setDate(String date) {
        this.date = date;
    }
    public int getPubId() {
        return pubId;
    }
    public void setPubId(int pubId) {
        this.pubId = pubId;
    }
    public double getCost() {
        return cost;
    }
    public void setCost(double cost) {
        this.cost = cost;
    }
    public double getRetail() {
        return retail;
    }
    public void setRetail(double retail) {
        this.retail = retail;
    }
    public double getDiscount() {
        return discount;
    }
    public void setDiscount(double discount) {
        this.discount = discount;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public Authors getAuthor() {
        return author;
    }
    public void setAuthor(Authors author) {
        this.author = author;
    }
    public Publisher getPublisher() {
        return publisher;
    }
    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }
}
