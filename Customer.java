public class Customer {
    private String id;
    private String lastName;
    private String firstName;
    private Address address;
    private String referred;
    public Customer(String id, String lastName, String firstName, Address address, String referred) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.address = address;
        this.referred = referred;
    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }
    public String getReferred() {
        return referred;
    }
    public void setReferred(String referred) {
        this.referred = referred;
    }
    
    
}
